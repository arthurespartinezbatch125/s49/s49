import React, {useState} from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

// Context
import UserContext from './UserContext';

/*components*/
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
// import CourseCard from './components/CourseCard';
// import Welcome from './components/Welcome'
import Courses from './pages/Courses';
// import Counter from './components/Counter'
import Register from './pages/Register';
import LogIn from './pages/LogIn';
import Error from './components/Error'

export default function App(){

	const [user, setUser] = useState({email: localStorage.getItem('email')});

	const unsetUser =() => {
		localStorage.clear();
		setUser({email: null})
	}

	return(
		// <Fragment>
		//   <AppNavbar/>
		//   <Home/>
		//   <CourseCard/>
		//  <Welcome name="John"/>
		//   <Welcome name="Lawrence"/>
		//   <Courses/>
		//   <Counter/>
		//  <Register/>
		//  <LogIn/>
		// </Fragment>
	<UserContext.Provider value={{user, setUser, unsetUser}}>
		<BrowserRouter>
			<AppNavbar/>				{/*//first user is props from APpnavbar}*/}
			<Switch>
				<Route exact path ="/" component={Home} />
				<Route exact path ="/courses" component={Courses} />
				<Route exact path ="/register" component={Register} />
				<Route exact path ="/login" component={LogIn} />
				<Route component={Error}/>
			</Switch>
		</BrowserRouter>
	</UserContext.Provider>
	)
}