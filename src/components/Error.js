import React from 'react';
// import {Link} from 'react-router-dom';

import {
	Container,
	Row,
	Col,
	Jumbotron,
} from 'react-bootstrap';

export default function Error(){

	return(
		<Container fluid>
			<Row>
				<Col className="px-0">
					<Jumbotron fluid className="px-3">
					  <h1>404-Not found</h1>
					  <p>The page you are looking for cannot be found</p>
					  <a href="/">Back Home</a>
					 {/* <Link to="/">Homepage</Link>*/}
					</Jumbotron>
				</Col>
			</Row>
		</Container>
	)
}
